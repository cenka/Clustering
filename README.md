# Data source:

#### FCPS:

A. Ultsch. Clustering with SOM: U*C. Workshop on Self-Organizing Maps, 
p. 75--82. WSOM, 2005.

```
@incollection{Ultsch2005:fcps,
   author="A. Ultsch",
   title="{C}lustering with {SOM}: {U*C}",
   year="2005",
   booktitle = "Workshop on Self-Organizing Maps",
   pages = "75--82",
}
```

https://www.uni-marburg.de/fb12/arbeitsgruppen/datenbionik/data?language_sync=1

#### PG:

D. Graves, W. Pedrycz. Kernel-based fuzzy clustering: A comparative experimental
study. Fuzzy Sets and Systems, 161:522–543, 2010.

```
@article{GravesPedrycz2010:kernelfuzzyclust,
    author = {Daniel Graves and Witold Pedrycz},
    journal = {Fuzzy Sets and Systems},
    number = {161},
    pages = {522--543},
    title = {Kernel-based fuzzy clustering:{A} comparative experimantal study},
    year = {2010}
}
```

#### SIPU:

Chosen from:

P. Franti, O. Virmajoki, Iterative shrinking method for clustering problems, Pattern Recognition, 39(5), 2006, pp. 761-765.

```
@article{FrantiVirmajoki2006:smcp,
   author = "P. Franti and O. Virmajoki",
   title = "Iterative shrinking method for clustering problems", 
   journal = "Pattern Recognition",
   volume = "39",
   number = "5",
   year = "2006", 
   pages = "761--765"
}
```

http://cs.joensuu.fi/sipu/datasets/


Described in:

M. Gagolewski, M. Bartoszuk, A. Cena. Genie: A new, fast, and outlier-resistant
hierarchical clustering algorithm. Information Sciences, 363:8–23, 2016.

```
@article{GagolewskiETAL2016:genie,
   author = "Marek Gagolewski and Maciej Bartoszuk and Anna Cena",
   title = "Genie: A new, fast, and outlier-resistant hierarchical clustering algorithm", 
   journal = "Information Sciences",
   volume = "363",
   year = "2016", 
   pages = "8--23"
}
```

http://www.gagolewski.com/resources/data/clustering/


